package org.bitbucket.sasynkam.primefacesapp.util;

import org.bitbucket.sasynkam.primefacesapp.model.Dropdown;
import org.bitbucket.sasynkam.primefacesapp.model.DropdownType;
import org.bitbucket.sasynkam.primefacesapp.service.DropdownService;

import java.util.ArrayList;
import java.util.List;


public class DropdownTagLib {

    public static List<Dropdown> getList(DropdownType type, DropdownService dropdownService) {
        return dropdownService.getDropdown(type);
    }

    public static List<Dropdown> getDependentList(DropdownType type, final String parentId, DropdownService dropdownService) {
        List<Dropdown> dropdown = dropdownService.getDropdown(type);
        if(parentId == null || parentId.isEmpty()) {
            return dropdown;
        }
        List<Dropdown> dependentDropdown = new ArrayList<>();
        for (Dropdown dropdownItem : dropdown) {
            if(dropdownItem.getParentId() != null && dropdownItem.getParentId().equalsIgnoreCase(parentId)) {
                dependentDropdown.add(dropdownItem);
            }
        }
        return dependentDropdown;
    }
}
