package org.bitbucket.sasynkam.primefacesapp.controller;

import org.bitbucket.sasynkam.primefacesapp.service.DropdownService;

import javax.faces.annotation.FacesConfig;
import javax.inject.Inject;
import java.io.Serializable;

import static javax.faces.annotation.FacesConfig.Version.JSF_2_3;

@FacesConfig(version = JSF_2_3) // Activates CDI build-in beans
public abstract class BaseController implements Serializable {

    @Inject
    private DropdownService dropdownService;

    public DropdownService getDropdownService() {
        return dropdownService;
    }
}
