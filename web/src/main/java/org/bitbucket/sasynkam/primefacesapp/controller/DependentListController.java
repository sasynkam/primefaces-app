package org.bitbucket.sasynkam.primefacesapp.controller;

import javax.enterprise.context.RequestScoped;
import javax.faces.annotation.FacesConfig;
import javax.inject.Named;
import java.time.LocalDateTime;

import static javax.faces.annotation.FacesConfig.Version.JSF_2_3;

@Named
@RequestScoped
@FacesConfig(version = JSF_2_3) // Activates CDI build-in beans
public class DependentListController extends BaseController {

    private String country;
    private String city;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void actionDisplayLocation() {
        System.out.println("Debug: DependentListController.actionDisplayLocation() Country " + country + ", City - " + city + " - " + LocalDateTime.now());
    }
}
