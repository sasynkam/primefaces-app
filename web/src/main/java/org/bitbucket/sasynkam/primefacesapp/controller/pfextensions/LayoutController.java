package org.bitbucket.sasynkam.primefacesapp.controller.pfextensions;

import org.primefaces.extensions.model.layout.LayoutOptions;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class LayoutController implements Serializable {

    private LayoutOptions layoutOptions;

    @PostConstruct
    private void init() {
        layoutOptions = new LayoutOptions();

        // options for all panes (center and west)
        LayoutOptions panes = new LayoutOptions();
        panes.addOption("slidable", false);
        panes.addOption("resizeWhileDragging", true);
        layoutOptions.setPanesOptions(panes);

        // options for west pane
        final LayoutOptions west = new LayoutOptions();
        west.addOption("size", 500);
        west.addOption("minSize", 200);
        west.addOption("maxSize", 700);
        layoutOptions.setWestOptions(west);
    }

    public LayoutOptions getLayoutOptions() {
        return layoutOptions;
    }

    public void setLayoutOptions(LayoutOptions layoutOptions) {
        this.layoutOptions = layoutOptions;
    }
}
