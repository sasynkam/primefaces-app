package org.bitbucket.sasynkam.primefacesapp.controller.primefaces;

import org.bitbucket.sasynkam.primefacesapp.service.websocket.WebsocketService;

import javax.faces.annotation.FacesConfig;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

import static javax.faces.annotation.FacesConfig.Version.JSF_2_3;

@Named
@ViewScoped
@FacesConfig(version = JSF_2_3) // Activates CDI build-in beans
public class WebSocketController implements Serializable {

    // This "link" is needed to initialize WebsocketService instance, thus @PostConstruct there
    @Inject
    private WebsocketService websocketService;

    private String label = "some label to initialize WebsocketService via JSF controller";

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
