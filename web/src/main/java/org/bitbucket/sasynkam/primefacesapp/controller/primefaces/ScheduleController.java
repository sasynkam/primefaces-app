package org.bitbucket.sasynkam.primefacesapp.controller.primefaces;

import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleModel;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Locale;

@Named
@ViewScoped
public class ScheduleController implements Serializable {

    private ScheduleModel eventModel;

    private Locale locale = new Locale("no", "NO");
    // private Locale locale = new Locale("sv", "SE");
    // private Locale locale = new Locale("en", "US");
    private ZoneId timeZone = ZoneId.systemDefault();
    private String clientTimeZone = "local";

    @PostConstruct
    public void init() {
        eventModel = new DefaultScheduleModel();

        DefaultScheduleEvent event = DefaultScheduleEvent.builder()
                .title("Birthday Party")
                .startDate(today1Pm())
                .endDate(today6Pm())
                .description("Aragon")
                .overlapAllowed(true)
                .build();
        eventModel.addEvent(event);
    }

    public ScheduleModel getEventModel() {
        return eventModel;
    }

    public void setEventModel(ScheduleModel eventModel) {
        this.eventModel = eventModel;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public ZoneId getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(ZoneId timeZone) {
        this.timeZone = timeZone;
    }

    public String getClientTimeZone() {
        return clientTimeZone;
    }

    public void setClientTimeZone(String clientTimeZone) {
        this.clientTimeZone = clientTimeZone;
    }

    private LocalDateTime today1Pm() {
        return LocalDateTime.now().withHour(13).withMinute(0).withSecond(0).withNano(0);
    }

    private LocalDateTime today6Pm() {
        return LocalDateTime.now().withHour(18).withMinute(0).withSecond(0).withNano(0);
    }
}
