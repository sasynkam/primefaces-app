module primefaces.app.web {
    // internal
    requires primefaces.app.model;
    requires primefaces.app.service;
    // external
    requires java.annotation;
    requires javax.inject;
    requires cdi.api;
    requires myfaces.api;
    requires primefaces;
    requires primefaces.extensions;
    requires com.google.gson;
    requires bootstrap;
}