module primefaces.app.repository {
    requires primefaces.app.model;
    requires javax.inject;
    exports org.bitbucket.sasynkam.primefacesapp.repository;
}