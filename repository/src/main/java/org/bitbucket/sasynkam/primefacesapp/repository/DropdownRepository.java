package org.bitbucket.sasynkam.primefacesapp.repository;

import org.bitbucket.sasynkam.primefacesapp.model.Dropdown;

import javax.inject.Named;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * DropdownRepository data should be loaded just once and then cached.
 * Some reload mechanism can be implemented too.
 */
@Named
public class DropdownRepository {

    private static List<Dropdown> countries;
    private static List<Dropdown> cities;

    static {
        countries = new ArrayList<>();
        Dropdown usa = new Dropdown("USA", "United States of America");
        countries.add(usa);
        Dropdown ger = new Dropdown("GERMANY", "Germany");
        countries.add(ger);
        Dropdown bra = new Dropdown("BRAZIL", "Brazil");
        countries.add(bra);
        Dropdown cze = new Dropdown("CZECHIA", "Czechia");
        countries.add(cze);
    }

    static {
        cities = new ArrayList<>();
        Dropdown ny = new Dropdown("New York", "New York", "USA");
        cities.add(ny);
        Dropdown sf = new Dropdown("San Francisco", "San Francisco", "USA");
        cities.add(sf);
        Dropdown dr = new Dropdown("Denver", "Denver", "USA");
        cities.add(dr);

        Dropdown bn = new Dropdown("Berlin", "Berlin", "GERMANY");
        cities.add(bn);
        Dropdown mh = new Dropdown("Munich", "Munich", "GERMANY");
        cities.add(mh);
        Dropdown ft = new Dropdown("Frankfurt", "Frankfurt", "GERMANY");
        cities.add(ft);

        Dropdown sp = new Dropdown("Sao Paulo", "Sao Paulo", "BRAZIL");
        cities.add(sp);
        Dropdown rj = new Dropdown("Rio de Janerio", "Rio de Janerio", "BRAZIL");
        cities.add(rj);
        Dropdown sr = new Dropdown("Salvador", "Salvador", "BRAZIL");
        cities.add(sr);

        Dropdown czPrag = new Dropdown("Prag", "Prag", "CZECHIA");
        cities.add(czPrag);
        Dropdown czOstrava = new Dropdown("Ostrava", "Ostrava", "CZECHIA");
        cities.add(czOstrava);
    }

    public List<Dropdown> getCountries() {
        System.out.println("Debug: DropdownRepository.getCountries() - " + LocalDateTime.now());
        return countries;
    }

    public List<Dropdown> getCities() {
        return cities;
    }
}