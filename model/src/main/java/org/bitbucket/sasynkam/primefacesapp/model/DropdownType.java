package org.bitbucket.sasynkam.primefacesapp.model;

public enum DropdownType {
    Countries,
    Cities;
}
