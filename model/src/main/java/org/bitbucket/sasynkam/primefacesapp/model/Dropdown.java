package org.bitbucket.sasynkam.primefacesapp.model;

import java.util.Objects;

public class Dropdown {

    private String id;
    private String name;
    private String parentId;


    public Dropdown(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Dropdown(String id, String name, String parentId) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getParentId() {
        return parentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dropdown dropdown = (Dropdown) o;
        return id.equals(dropdown.id) &&
                Objects.equals(name, dropdown.name) &&
                Objects.equals(parentId, dropdown.parentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, parentId);
    }
}
