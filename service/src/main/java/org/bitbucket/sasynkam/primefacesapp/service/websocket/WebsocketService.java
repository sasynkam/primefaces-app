package org.bitbucket.sasynkam.primefacesapp.service.websocket;

import javax.annotation.PostConstruct;
import javax.faces.push.Push;
import javax.faces.push.PushContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Named
public class WebsocketService implements Serializable {

    @Inject
    @Push(channel = "pushChannel")
    private PushContext pushChannel;

    final String userName = "sasynkam";

    @PostConstruct
    public void init() {
        System.out.println("Debug: WebsocketService.init() - " + LocalDateTime.now());
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleWithFixedDelay(() -> sendPushMessage(userName), 5, 5, TimeUnit.SECONDS);
    }

    public void sendPushMessage(final String userName) {
        System.out.println("Debug: WebsocketService.sendPushMessage() - " + LocalDateTime.now());
        pushChannel.send("Hello from server", userName);
    }
}
