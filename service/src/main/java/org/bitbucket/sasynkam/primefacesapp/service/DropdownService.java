package org.bitbucket.sasynkam.primefacesapp.service;

import org.bitbucket.sasynkam.primefacesapp.model.Dropdown;
import org.bitbucket.sasynkam.primefacesapp.model.DropdownType;
import org.bitbucket.sasynkam.primefacesapp.repository.DropdownRepository;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
public class DropdownService {

    @Inject
    private DropdownRepository dropdownRepository;

    public List<Dropdown> getDropdown(DropdownType type) {
        switch (type) {
            case Countries: return dropdownRepository.getCountries();
            case Cities: return dropdownRepository.getCities();
            default: return new ArrayList<>();
        }
    }
}
