module primefaces.app.service {
    // internal
    requires primefaces.app.common;
    requires primefaces.app.model;
    requires primefaces.app.repository;
    // external
    requires javax.inject;
    requires myfaces.api;
    requires java.annotation;

    exports org.bitbucket.sasynkam.primefacesapp.service;
    exports org.bitbucket.sasynkam.primefacesapp.service.websocket;
}